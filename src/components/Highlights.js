
import {Row, Col} from 'react-bootstrap';
import Cards from './Card';
import {Fragment} from 'react'
const Highlights = () => {
	return(
		<Fragment>
			<Row className="mt-3 mb-3">
				<Col xs = {12} md = {4}>
					<Cards title="Learn From Home" subject="Lorem ipsum dolor sit amet consectetur adipisicing elit. Est perferendis rerum, harum cumque rem dolores sint, repellendus? Dolores, assumenda exercitationem, cumque voluptatum sapiente at similique fugit fuga, quo, repudiandae quas!"/>
				</Col>
				<Col xs = {12} md = {4}>
					<Cards title="Study Now Pay later" subject="Lorem ipsum dolor sit amet consectetur adipisicing elit. Est perferendis rerum, harum cumque rem dolores sint, repellendus? Dolores, assumenda exercitationem, cumque voluptatum sapiente at similique fugit fuga, quo, repudiandae quas!"/>
				</Col>
				<Col xs = {12} md = {4}>
					<Cards title="Be part of our Community" subject="Lorem ipsum dolor sit amet consectetur adipisicing elit. Est perferendis rerum, harum cumque rem dolores sint, repellendus? Dolores, assumenda exercitationem, cumque voluptatum sapiente at similique fugit fuga, quo, repudiandae quas!"/>
				</Col>
			</Row>
		</Fragment>
	)
}

export default Highlights;