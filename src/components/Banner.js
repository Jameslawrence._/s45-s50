import {Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Banner = ({data}) => {
	const {header, content, btnType, buttonContent, destination} = data;
	console.log(data)
	return(
		<div className="p-3">
			<h3>{header}</h3>
			<p>{content}</p>
			<Button  as={Link} to={destination} variant={btnType}>{buttonContent}</Button>
		</div>	
	)
}

export default Banner; 