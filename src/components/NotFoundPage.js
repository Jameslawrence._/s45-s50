import Banner from './Banner';

export default function NotFoundPage(){

	const data = {
		header: "404 - Page Not Found",
		content: "Oops! The link the you are looking for does not exist!",
		btnType: "danger",
		buttonContent: "Go Back",
		destination: "/"
	}

	return(
		<div>
			<Banner data={data}/>
		</div>
	)
}