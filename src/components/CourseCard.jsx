import React, {useState,useContext} from 'react';
import {Button} from 'react-bootstrap';
import UserContext from '../userContext';
import {Link} from 'react-router-dom';

const CourseCard = ({courseProp}) => {
	const {name, description, price, _id} = courseProp
	const {user} = useContext(UserContext);

	console.log(user.id);
	const [count, setCount] = useState(0)
	const [seats, setSeats] = useState(30)
	const enroll = () => {
			if(seats < 1){
				alert("Course Full");
			}else{
				setCount(count + 1);
				setSeats(seats - 1);
			}
	}

	return(
		<div className="course-container">
			<h3>{name}</h3>
			<h5>Description</h5>
			<p>{description}</p>
			<h5>Price</h5>
			<p>Php {price}</p>
			<p>Enrollees: {count}</p>
			<p>Available Seats: {seats}</p>			
			{user.id?<Button onClick={enroll}variant="primary">Enroll</Button>:null}
			<Button as={Link} to={`/courses/${_id}`} exact="true">Details</Button>
			{/*<Link className = "btn btn-primary" to = "/courseView">Details</Link>*/}
		</div>
	)
}

export default CourseCard;