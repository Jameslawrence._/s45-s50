
import {Card} from 'react-bootstrap';
import {Fragment} from 'react';

const Cards = (props) => {
	return(
		<Fragment>
			<Card className="cardHighLight p-3">
				<Card.Body>
					<Card.Title>
						<h2>{props.title}</h2>
					</Card.Title>
					<Card.Text>
						<p>{props.subject}</p>
					</Card.Text>
				</Card.Body>
			</Card>
		</Fragment>
	)
}

export default Cards;