//import Navbar from 'react-bootstrap/Navbar';
//import Nav from 'react-bootstrap/Nav';
import {Navbar, Nav} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import {Fragment, useContext} from 'react';
import UserContext from '../userContext';
const AppNavbar = () => {
/*	// State to store the user information stored in login page. 
	const [user, setUser] = useState(localStorage.getItem("email"));
	console.log(user); */
	const {user} = useContext(UserContext);
	console.log(user.id)
	return(
		<Fragment>
			<Navbar bg="light" expand="md">
			  <Navbar.Brand as={Link} to="/" exact="true">Zuitt</Navbar.Brand>
			  <Navbar.Toggle aria-controls="basic-navbar-nav" />
			  <Navbar.Collapse id="basic-navbar-nav">
			    <Nav className="mr-auto">
			      <Nav.Link as={Link} to="/" exact="true">Home</Nav.Link>
			      <Nav.Link as={Link} to="/courses" exact="true">Courses</Nav.Link>
			    </Nav>
			    <Nav className="ms-auto">
			    {(localStorage.getItem('token'))?  <Nav.Link as={Link} to="/logout" exact="true">Logout</Nav.Link>
					:   <Fragment>
					      <Nav.Link as={Link} to="/register" exact="true">Signup</Nav.Link>
					      <Nav.Link as={Link} to="/login" exact="true">Login</Nav.Link>
					  	</Fragment>
			    }
			    </Nav>
			  </Navbar.Collapse>
			</Navbar>			
		</Fragment>
	)
}

export default AppNavbar;
