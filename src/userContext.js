import React from 'react';
/* 
	A create context is a special React Object which will allow us to store information 
	within and pass it around our components within the app.

	The Context Object is a different approach to passing information between components without 
	the need to pass props from component to component.
*/
const UserContext = React.createContext();	
/*
	Provider component allows other components to consume or use the context object and supply the necessary 
	information needed to the context object.
*/
export const UserProvider = UserContext.Provider;
export default UserContext;