import React, {Fragment, useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
//import UserContext from '../userContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

// note react-router-dom -v6 uses Navigate instead of Redirect. 
// v6 Switch is now Called Routes
// v6 component is now called element ex.element = {<Home/>}


export default function Register(){
	//const {user, setUser} = useContext(UserContext);	
	//State Hooks to store the values of the Input Fields
	//const [password1, setPassword1] = useState('')
	//const [password2, setPassword2] = useState('')
	//const [email, setEmail] = useState('')
	const[userCredentials, setUserCredentials] = useState({
		firstName: "",
		lastName: "",
		mobileNo: "",
		email: "",
		password: "",
		password2: ""
	})
	// State to determine whether the register button is enable or not
	const [isActive, setIsActive] = useState(false)
	// UseEffect
	const {email, password, password2} = userCredentials
	useEffect(() => {
		if((email && password && password2)&&(password === password2)){
			setIsActive(true)
		}else setIsActive(false)
	}, [email, password, password2])


	const registerUser = (event) => {
		event.preventDefault();
		const { firstName, lastName, email, mobileNo, password } = userCredentials;
		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			body: JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				email : email,
				mobileNo : mobileNo,
				password : password
			}),
			headers:{
				'Content-Type': 'application/json'
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data){
				Swal.fire({
				  title: 'Welcome!',
				  text: 'Successfully Registered',
				  icon: 'success',
				  confirmButtonText: 'Welcome to Zuitt Bootcamp'
				})				
			}else{
				Swal.fire({
				  title: 'Error!',
				  text: 'Failed to register',
				  icon: 'error',
				  confirmButtonText: 'Check your register details'
				})					
			}
		})
//		localStorage.setItem('email', userCredentials.email);
/*		setUser({
			email: localStorage.getItem('email')
		})*/
		setUserCredentials(() => {
			return{
				firstName: "",
				lastName: "",
				mobileNo: "",
				email: "",
				password: "",
				password2: ""
			}
		})
	}
	

	const onChangeHandler = (event) => {
		const {value, name} = event.target
		return setUserCredentials(prevValue => {
			return {
				...prevValue, 
				[name]: value 
			}
		})
	}
	return(
		<Fragment>
			{(localStorage.getItem('token'))? <Navigate to='/courses'/>: 
			<Form onSubmit={registerUser}>
				<Form.Group>
					<Form.Label>Firstname</Form.Label>
					<Form.Control
						value={userCredentials.firstName}
						//value={email}
						type="text"
						//onChange={e => setEmail(e.target.value)}
						onChange={onChangeHandler}
						placeholder="Enter your Firstname"
						required
						name="firstName"
					></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Lastname</Form.Label>
					<Form.Control
						value={userCredentials.lastName}
						//value={email}
						type="text"
						//onChange={e => setEmail(e.target.value)}
						onChange={onChangeHandler}
						placeholder="Enter your Lastname"
						required
						name="lastName"
					></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						value={userCredentials.mobileNo}
						//value={email}
						type="text"
						//onChange={e => setEmail(e.target.value)}
						onChange={onChangeHandler}
						placeholder="(+63)"
						required
						name="mobileNo"
					></Form.Control>
				</Form.Group>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						value={userCredentials.email}
						//value={email}
						type="email"
						//onChange={e => setEmail(e.target.value)}
						onChange={onChangeHandler}
						placeholder="Please enter your email"
						required
						name="email"
					></Form.Control>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>
				<Form.Group controlId="password">
					<Form.Label>Password:</Form.Label>
					<Form.Control
						value={userCredentials.password}
						//value={password1}
						type="password"
						placeholder="Please enter password"
						required
						onChange={onChangeHandler}
						//onChange={e => setPassword1(e.target.value)}
						name="password"
					></Form.Control>
				</Form.Group>
				<Form.Group controlId="password2">
					<Form.Label>Verify Password:</Form.Label>
					<Form.Control
						value={userCredentials.password2}
						//value={password2}
						type="password"
						placeholder="Please verify password"
						required
						onChange={onChangeHandler}
						name="password2"
						//onChange={e => setPassword2(e.target.value)}
					></Form.Control>
				</Form.Group>
				{isActive? 
					<Button type="submit" onClick={registerUser} variant = "primary" id="submitBtn">Register</Button> 
					:
					<Button type="submit" variant = "danger" id="submitBtn" disabled>Register</Button>
				}
			</Form>
		}
		</Fragment>
	)
}