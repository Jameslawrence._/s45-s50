import { Navigate } from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../userContext';
export default function Logout(){
//	localStorage.clear();
	const {unsetUser, setUser} = useContext(UserContext);

	// clear the local storage.
	unsetUser();
	
	useEffect(() => {
		setUser({id:null})
	})
	return(
		<Navigate to="/login"/>
	)
}