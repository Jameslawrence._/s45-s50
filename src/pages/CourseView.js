import { useState, useEffect, useContext} from 'react';
import {Container, Row, Card, Button, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
// useHistory for v5 down in react router
// useNavigate for v6
import UserContext from '../userContext';

export default function CourseView(props){
	
	//use params scope to retrieve the course id passed via Url.
	const {courseId} = useParams()

	const {user} = useContext(UserContext);

	// useHistory or useNavigate log us to redirect user to a different page.
	const history = useNavigate()
	
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	

	const enroll = (courseId) => {
		fetch(`http://localhost:4000/users/enroll`,{
			method: 'POST',
			headers:{
				"Content-Type": "application/json",
				Authorization : `Bearer ${localStorage.getItem("token")}` 
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: user.id
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true){
				Swal.fire({
				  title: 'Welcome!',
				  text: 'Successfully Enrolled',
				  icon: 'success',
				  confirmButtonText: 'Enjoy Studying'
				})

				history("/courses");
			}else{
				Swal.fire({
				  title: 'Error!',
				  text: 'Authentication Failed',
				  icon: 'error',
				  confirmButtonText: 'Retry'
				})				
			}
		})

	}

	useEffect(() => {
		console.log(courseId)
		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [courseId])

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
                    		<Card.Title>{name}</Card.Title>
                    		<Card.Subtitle>Description:</Card.Subtitle>
                    		<Card.Text>{description}</Card.Text>
                    		<Card.Subtitle>Price</Card.Subtitle>
                    		<Card.Text>Php {price}</Card.Text>
                    		<Card.Subtitle>Class Schedule</Card.Subtitle>
                    		<Card.Text>5:30 PM to 9:30 PM</Card.Text>
                    		{user.id? <Button onClick={()=> enroll(courseId)} variant="primary">Enroll</Button>
                    		: <Button as={Link} to='/login' exact="true" variant="danger">Login to Enroll</Button>
                    		}  
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}