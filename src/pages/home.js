import {Fragment} from 'react';
import Highlights from '../components/Highlights';
import Banner from '../components/Banner';

const Home = () => {
	const data = {
		header:"Zuitt Coding Bootcamp",
		content:"Opportunities are for everyone and everywhere", 
		btnType:"primary",
		buttonContent:"Enroll Now!",
		destination: "/register"
	}

	return(

		<Fragment>
			<Banner data={data}/>
			<Highlights/>
		</Fragment>
	)
}

export default Home;