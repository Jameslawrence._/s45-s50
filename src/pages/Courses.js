import {Fragment, useEffect, useState} from 'react';
//import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	

	const [courses, setCourses] = useState([]);
	useEffect(() => {
		fetch('http://localhost:4000/courses/all')
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
					return <CourseCard key={course._id} courseProp={course}/>
			}))			
		})
	}, []);
	return(
		<Fragment>
			<h2>Courses</h2>
{/*			<CourseCard courseProp = {coursesData[0]}/>*/}
			{courses}
		</Fragment>
	)
}