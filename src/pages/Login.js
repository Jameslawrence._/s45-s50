import {Fragment ,useState, useEffect, useContext} from 'react';
import {Form, Button, Alert} from 'react-bootstrap';
import UserContext from '../userContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(){
	/*
		Allow us to consume the user context object and it's properties to use for user validation 
	*/
	const {user, setUser} = useContext(UserContext);

	const [userCredentials, setUserCredentials] = useState({
		email:"",
		password:"",
	}); 
	
	const [isFilled, setIsFilled] = useState();
	const [show, setShow] = useState(false);

	const {email, password} = userCredentials 
	useEffect(() => {
		if(email && password) setIsFilled(true)
		else setIsFilled(false)	
	},[email, password])

	const loginUser = (event) => {
		event.preventDefault()
		
		//fetch the email in the backend if exist.
		fetch('http://localhost:4000/users/login',{
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access);

				Swal.fire({
				  title: 'Welcome!',
				  text: 'Successfully Login',
				  icon: 'success',
				  confirmButtonText: 'Welcome to Zuitt Bootcamp'
				})
			}else{
				Swal.fire({
				  title: 'Error!',
				  text: 'Authentication Failed',
				  icon: 'error',
				  confirmButtonText: 'Check your login details'
				})				
			} 

		})

		// Set email of user in the local storage.
		// localStorage.setItem('propertyName', value);
		//localStorage.setItem('email', userCredentials.email);
		/*
		setUser({
			email: localStorage.getItem('email')
		})
		*/
		setShow(true, window.setTimeout(() => {setShow(false)}, 1000));
		setUserCredentials(() => {return{email:"", password:""}})
	}

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/details',{
			headers:{
				Authorization:`Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}	

	const onChangeHandler = (event) => {
		const {value, name} = event.target;
		setUserCredentials((prevValue) =>{
			return{
				...prevValue,
				[name]:value 
			}	
		})
	}
	return(
		<Fragment>
			

			{(user.id)? 
				<Fragment>
					<Alert show={show} variant="success">
						<Alert.Heading>LoggedIn Successfully</Alert.Heading>
					</Alert>
					<Navigate to='/courses'/>
				</Fragment>:	 	
			<Form onSubmit={loginUser}>
			  <Form.Group controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control onChange={onChangeHandler} name="email" type="email" value={user.email} placeholder="Enter email" />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control onChange={onChangeHandler} name="password" value={user.password} type="password" placeholder="Password" />
			  </Form.Group>
			  {isFilled? <Button variant="primary" type="submit"> Submit </Button> :
			   <Button variant="primary" onClick={loginUser} type="submit" disabled> Submit </Button>} 
			</Form>
			}
		</Fragment>
	)
}