
const coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio, esse deserunt sint repellendus",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Phyton Jango",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio, esse deserunt sint repellendus",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio, esse deserunt sint repellendus",
		price: 60000,
		onOffer: true
	},
]

export default coursesData;