import {Container} from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFoundPage from './components/NotFoundPage.js';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import { UserProvider } from './userContext';
import './App.css';
const App = () => {
	// State hook for the userState that's define here for a global scope.
	// Initialize as an object with properties from the local storage.
	// This will be used to storage the information and will be used for validating if a user  login 
	// on the app or not.

	const [user, setUser] = useState({
		//email: localStorage.getItem('email')
		id: null,
		isAdmin: null
	});

	// Function for clearing local storage.	
	const unsetUser = () => {
		localStorage.clear()
	}

	useEffect(() => {
		console.log(user);
		console.log(localStorage);
	}, [user]);
	/*
		The user provider component is what allows other components to consume or use our context.
		Any component which is not wrapped by userProvider will not have access to the values provided 
		for our context 
		
		You can pass data or information to our context by providing a value attribute in our userProvider.
		Data passed here can be accessed by other components by unwrapping our context using the use context 
		Hook.
	*/
	return(
		<UserProvider value={{user, setUser, unsetUser}}>
			<Router>
				<AppNavbar/>
				<Container>
					<Routes>
						<Route exact="true" path='/' element={<Home/>}/>
						<Route exact="true" path='/courses' element={<Courses/>}/>
						<Route exact="true" path='/courses/:courseId' element={<CourseView/>}/>
						<Route exact="true" path='/register' element={<Register/>}/>
						<Route exact="true" path='/login' element={<Login/>}/>		
						<Route exact="true" path='/logout' element={<Logout/>}/>
						<Route path='*' element={<NotFoundPage/>}/>
					</Routes>				
				</Container>
			</Router>
		</UserProvider>
	)
}
export default App;

/*
	React JS is a single page application(SPA). However, we can simulate the changing of pages. 
	We don't actually create new pages, what we just do is switch what needs to be render on a specific page according
	to their assigned routes. React JS and react-router-dom package just mimics or mirrors how HTML 
	access its URL.

	react-router-dom 3 main components to simulate the changing of page. 

	1. Router - wrapping the router component around other components will allow us to use routing within our page.
	2. Switch - Allows us to switch/ change our page components.
	3. Route - assigns a path which will trigger the change/switch of components render. 

*/